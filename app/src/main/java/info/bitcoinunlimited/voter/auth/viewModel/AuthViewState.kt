package info.bitcoinunlimited.voter.auth.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData

data class AuthViewState(
    val qrCodeUri: LiveData<QrCodeResult> = liveData { }
)

data class QrCodeResult(
    val qrCodeUri: String? = null
)
