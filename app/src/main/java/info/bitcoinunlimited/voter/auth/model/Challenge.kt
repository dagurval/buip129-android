package info.bitcoinunlimited.voter.auth.model

data class Challenge(
    val value: String
)
