package info.bitcoinunlimited.voter.auth.model

import java.util.Date

// JSON Web Token
data class JsonWebToken(
    val value: String,
    val issuedAt: Date
)

data class SignInAnonymouslyResult(
    val userId: String?,
    val error: Exception?
)

data class AuthProviders(
    val anonymous: Boolean = false,
    val custom: Boolean = false
)
