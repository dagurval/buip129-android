package info.bitcoinunlimited.voter.auth.model.api

import com.google.gson.internal.LinkedTreeMap
import retrofit2.http.GET
import retrofit2.http.Query

interface AuthApi {
    @GET("/identify")
    suspend fun identify(
        @Query("op") operation: String,
        @Query("cookie") cookie: String,
        @Query("addr") address: String,
        @Query("sig") signature: String
    ): LinkedTreeMap<String, String>
}
