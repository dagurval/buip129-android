package info.bitcoinunlimited.voter.auth.viewModel
import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import info.bitcoinunlimited.voter.ElectrumAPI
import info.bitcoinunlimited.voter.UserRepository
import info.bitcoinunlimited.voter.WalletRepository
import info.bitcoinunlimited.voter.auth.AuthRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class AuthViewModelFactory(
    private val repository: AuthRepository,
    private val userRepository: UserRepository,
    private val electrumAPI: ElectrumAPI,
    private val walletRepository: WalletRepository,
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle? = null
) : AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(
        key: String,
        modelClass: Class<T>,
        handle: SavedStateHandle
    ): T {
        return AuthViewModel(repository, userRepository, walletRepository) as T
    }
}
