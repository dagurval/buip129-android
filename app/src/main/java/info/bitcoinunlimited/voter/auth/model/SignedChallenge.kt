package info.bitcoinunlimited.voter.auth.model

import bitcoinunlimited.libbitcoincash.Codec

data class SignedChallenge(
    val challenge: String,
    val signature: ByteArray,
    val address: String,
    val signatureBase64: String = Codec.encode64(signature)
) {
    constructor(challenge: Challenge, signature: ByteArray, address: String) : this(challenge.value, signature, address)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as SignedChallenge

        if (challenge != other.challenge) return false
        if (!signature.contentEquals(other.signature)) return false
        if (address != other.address) return false
        if (signatureBase64 != other.signatureBase64) return false

        return true
    }

    override fun hashCode(): Int {
        var result = challenge.hashCode()
        result = 31 * result + signature.contentHashCode()
        result = 31 * result + address.hashCode()
        result = 31 * result + signatureBase64.hashCode()
        return result
    }
}
