package info.bitcoinunlimited.voter.auth.viewModel

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import bitcoinunlimited.libbitcoincash.PayDestination
import info.bitcoinunlimited.voter.UserRepository
import info.bitcoinunlimited.voter.WalletRepository
import info.bitcoinunlimited.voter.auth.AuthRepository
import info.bitcoinunlimited.voter.election.master.view.ElectionMasterFragment
import java.lang.Exception
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class AuthViewModel(
    private val authRepository: AuthRepository,
    private val userRepository: UserRepository,
    private val walletRepository: WalletRepository
) : ViewModel(), AuthEvents {
    private val logTag = AuthViewModel::class.java.simpleName
    val viewEffects: LiveData<AuthEffects> get() = _viewEffects
    private var _viewEffects = MutableLiveData(AuthEffects())
    val viewState: LiveData<AuthViewState> get() = _viewState
    private var _viewState = MutableLiveData(AuthViewState())

    fun observeAuth() {
        authRepository.authenticated.onEach {
            if (it) {
                val userId = authRepository.getCurrentUserId() ?: throw Exception("Cannot get userId")
                userRepository.updateNotificationToken(userId)
            }
        }.flowOn(Dispatchers.IO).launchIn(viewModelScope)
    }

    fun attachEvents(fragment: Fragment) {
        when (fragment) {
            is ElectionMasterFragment -> fragment.initAuthEvents(this)
        }
    }

    fun startAuth(context: Context) = GlobalScope.launch(Dispatchers.IO) {
        val walletRepository = WalletRepository.getInstance()
        walletRepository.initWallet(context)
        val identity = walletRepository.getIdentity()
        authenticate(identity)
    }

    private fun authenticate(identity: PayDestination) = runBlocking {
        if (!authRepository.isAuthenticated())
            authRepository.signInAnonymously()
        val challenge = authRepository.requestChallenge()
        val signedChallenge = authRepository.signChallenge(identity, challenge)
        val jsonWebToken = authRepository.identify(signedChallenge)
        authRepository.signInWithCustomJwtAndLinkAuths(jsonWebToken)
    }

    override fun clickToOpenQrScanner(event: AuthEventType.ClickToOpenQrCodeScanner) {
    }

    override fun qrCodeRead(event: AuthEventType.QrCodeRead) {
        val uri = event.uri
    }

    override fun startQrScanner(event: AuthEventType) {
        _viewEffects.send(AuthEffectType.StartQrCodeScanner)
    }
}
