package info.bitcoinunlimited.voter.auth.model.api

import com.google.gson.internal.LinkedTreeMap
import info.bitcoinunlimited.voter.auth.model.IdentifyRequest
import info.bitcoinunlimited.voter.utils.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class AuthRetriever {
    private val service: AuthApi

    companion object {
        private val BASE_URL = Constants.EndpointUrl
    }

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://europe-west1-voter-6700b.cloudfunctions.net/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        service = retrofit.create(AuthApi::class.java)
    }

    suspend fun identify(request: IdentifyRequest): LinkedTreeMap<String, String> {
        val operation = request.operation
        val cookie = request.cookie
        val address = request.address
        val signature = request.signature
        return service.identify(operation, cookie, address, signature)
    }
}
