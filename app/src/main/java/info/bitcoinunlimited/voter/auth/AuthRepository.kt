package info.bitcoinunlimited.voter.auth

import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.Wallet
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.FirebaseFunctionsException
import info.bitcoinunlimited.voter.auth.model.Challenge
import info.bitcoinunlimited.voter.auth.model.IdentifyRequest
import info.bitcoinunlimited.voter.auth.model.JsonWebToken
import info.bitcoinunlimited.voter.auth.model.SignInAnonymouslyResult
import info.bitcoinunlimited.voter.auth.model.SignedChallenge
import info.bitcoinunlimited.voter.auth.model.api.AuthRetriever
import info.bitcoinunlimited.voter.utils.Constants
import java.util.Date
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import retrofit2.HttpException

@ExperimentalCoroutinesApi
class AuthRepository {
    private fun functions(): FirebaseFunctions {
        return FirebaseFunctions.getInstance("europe-west1")
    }

    private fun auth() = FirebaseAuth.getInstance()

    private val _authenticated: MutableStateFlow<Boolean> = MutableStateFlow(false) // private mutable state flow
    val authenticated: StateFlow<Boolean> get() = _authenticated // publicly exposed as read-only state flow

    fun observeAuth() {
        auth().addAuthStateListener {
            _authenticated.value = it.currentUser != null
        }
    }

    suspend fun identify(signedChallenge: SignedChallenge) = suspendCoroutine<JsonWebToken> { continuation ->
        val cookie = auth().currentUser?.uid ?: throw Exception("Cannot get currentUserId")
        val identifyRequest = IdentifyRequest(
            "login",
            cookie,
            signedChallenge.address,
            signedChallenge.signatureBase64
        )

        val errorHandler = CoroutineExceptionHandler { _, exception ->
            when (exception) {
                is HttpException -> {

                    // NOTE: Workaround until signing is resolved
                    if (exception.code() == 418) {
                        println("exception.code() == 418")
                    } else {
                        println(exception)
                        FirebaseCrashlytics.getInstance().recordException(exception)
                    }
                }
                else -> {
                    FirebaseCrashlytics.getInstance().recordException(exception)
                    println(exception)
                    throw exception
                }
            }
        }

        GlobalScope.launch(errorHandler) {
            val result = AuthRetriever().identify(identifyRequest)
            val token = result["token"] ?: throw Exception("Cannot get jsonWebToken from identify:response")

            val jsonWebToken = JsonWebToken(
                token,
                Date()
            )
            continuation.resume(jsonWebToken)
        }
    }

    fun getCurrentUserId(): String? {
        return FirebaseAuth.getInstance().currentUser?.uid
    }

    suspend fun signInAnonymously() = suspendCoroutine<SignInAnonymouslyResult> { continuation ->
        GlobalScope.launch {
            try {
                val authResult = auth().signInAnonymously().await()
                val userId = authResult.user?.uid

                if (userId == null) {
                    val error = Exception("userId == null")
                    val errorResult =
                        SignInAnonymouslyResult(null, error)
                    FirebaseCrashlytics.getInstance().recordException(error)
                    continuation.resume(errorResult)
                } else
                    continuation.resume(
                        SignInAnonymouslyResult(
                            userId,
                            null
                        )
                    )
            } catch (error: Exception) {
                FirebaseCrashlytics.getInstance().recordException(error)
                continuation.resume(
                    SignInAnonymouslyResult(
                        null,
                        error
                    )
                )
            }
        }
    }

    suspend fun requestChallenge() = suspendCoroutine<Challenge> { continuation ->
        GlobalScope.launch(Dispatchers.Default) {
            try {
                val challenge = functions().getHttpsCallable(Constants.Firebase.Functions.RequestChallenge)
                    .call()
                    .continueWith { task ->
                        val challengeString = task.result?.data as String
                        Challenge(challengeString)
                    }.await()

                continuation.resume(challenge)
            } catch (error: FirebaseFunctionsException) {
                FirebaseCrashlytics.getInstance().recordException(error)
                throw error
            }
        }
    }

    fun signChallenge(identity: PayDestination, challenge: Challenge): SignedChallenge {
        val challengeBytes = challenge.value.toByteArray()
        val privateKey = identity.secret ?: throw Error("Cannot get privateKey")
        val address = identity.address.toString()
        val signature = Wallet.signMessage(challengeBytes, privateKey)
        return SignedChallenge(
            challenge,
            signature,
            address
        )
    }

    fun isAuthenticated(): Boolean {
        val currentUser = auth().currentUser
        return currentUser != null
    }

    fun isSignedIdAnonymously(): Boolean {
        return auth().currentUser?.isAnonymous ?: false
    }

    suspend fun verifySignedChallenge(signedChallenge: SignedChallenge) = suspendCoroutine<JsonWebToken> {
        continuation ->

        val inputData = hashMapOf(
            Constants.Firebase.Functions.PublicKey to signedChallenge.address,
            Constants.Firebase.Functions.Signature to signedChallenge.signatureBase64,
            Constants.Firebase.Functions.Challenge to signedChallenge.challenge
        )

        GlobalScope.launch {
            try {
                val jwtToken = functions().getHttpsCallable(Constants.Firebase.Functions.VerifySignedChallenge)
                    .call(inputData)
                    .continueWith { task ->
                        val resultData = task.result?.data as Map<*, *>
                        val jsonWebToken = resultData[Constants.Firebase.Functions.JsonWebToken] as String?
                        jsonWebToken
                    }.await()

                if (jwtToken == null) {
                    val errorMessage = "${Constants.Firebase.Functions.JsonWebToken} is null"
                    val error = Exception(errorMessage)
                    FirebaseCrashlytics.getInstance().recordException(error)
                    throw error
                } else
                    continuation.resume(JsonWebToken(jwtToken, Date()))
            } catch (error: FirebaseFunctionsException) {
                FirebaseCrashlytics.getInstance().recordException(error)
                throw error
            }
        }
    }

    suspend fun signInWithCustomJwtAndLinkAuths(jsonWebToken: JsonWebToken) {
        try {
            val customAuthResult = auth().signInWithCustomToken(jsonWebToken.value)
                .addOnFailureListener {
                    FirebaseCrashlytics.getInstance().recordException(it)
                }
                .await()

            val customAuthCredential = customAuthResult.credential ?: return
            auth().currentUser?.linkWithCredential(customAuthCredential)?.await()
            return
        } catch (error: Exception) {
            FirebaseCrashlytics.getInstance().recordException(error)
            return
        }
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: AuthRepository? = null

        fun getInstance() = instance ?: synchronized(this) {
            instance
                ?: AuthRepository()
                    .also { instance = it }
        }
    }
}
