package info.bitcoinunlimited.voter.auth.viewModel
import info.bitcoinunlimited.voter.auth.viewModel.AuthEventType.ClickToOpenQrCodeScanner
import info.bitcoinunlimited.voter.auth.viewModel.AuthEventType.QrCodeRead

interface AuthEvents {
    fun clickToOpenQrScanner(event: ClickToOpenQrCodeScanner)
    fun qrCodeRead(event: QrCodeRead)
    fun startQrScanner(event: AuthEventType)
}

sealed class AuthEventType {
    object ClickToOpenQrCodeScanner : AuthEventType()
    data class QrCodeRead(val uri: String) : AuthEventType()
    object StartQrScanner : AuthEventType()
}
