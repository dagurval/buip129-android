package info.bitcoinunlimited.voter.auth.model

data class IdentifyRequest(
    val operation: String,
    val cookie: String,
    val address: String,
    val signature: String
)
