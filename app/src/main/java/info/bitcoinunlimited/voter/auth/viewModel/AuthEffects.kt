package info.bitcoinunlimited.voter.auth.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import info.bitcoinunlimited.voter.auth.viewModel.AuthEffectType.AuthInBrowserSuccessEffect
import info.bitcoinunlimited.voter.auth.viewModel.AuthEffectType.OpenQrScannerEffect
import info.bitcoinunlimited.voter.auth.viewModel.AuthEffectType.StartQrCodeScanner
import info.bitcoinunlimited.voter.utils.livedata.Event

data class AuthEffects(
    val openQrScanner: LiveData<Event<OpenQrScannerEffect>> = liveData { },
    val authInBrowserSuccessEffect: LiveData<Event<AuthInBrowserSuccessEffect>> = liveData { },
    val startQrCodeScanner: LiveData<Event<StartQrCodeScanner>> = liveData { }
)

sealed class AuthEffectType {
    object OpenQrScannerEffect : AuthEffectType()
    object AuthInBrowserSuccessEffect : AuthEffectType()
    object StartQrCodeScanner : AuthEffectType()
}

fun MutableLiveData<AuthEffects>.send(effect: AuthEffectType) {
    this.value = when (effect) {
        is OpenQrScannerEffect ->
            this.value?.copy(openQrScanner = liveData { emit(Event(effect)) })
        is AuthInBrowserSuccessEffect ->
            this.value?.copy(authInBrowserSuccessEffect = liveData { emit(Event(effect)) })
        is StartQrCodeScanner ->
            this.value?.copy(startQrCodeScanner = liveData { emit(Event(effect)) })
    }
}
