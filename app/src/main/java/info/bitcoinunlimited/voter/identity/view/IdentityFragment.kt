package info.bitcoinunlimited.voter.identity.view

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter
import info.bitcoinunlimited.voter.InjectorUtils
import info.bitcoinunlimited.voter.databinding.FragmentIdentityBinding
import info.bitcoinunlimited.voter.identity.viewModel.IdentityViewEventType
import info.bitcoinunlimited.voter.identity.viewModel.IdentityViewEvents
import info.bitcoinunlimited.voter.identity.viewModel.IdentityViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class IdentityFragment : Fragment() {
    private var _binding: FragmentIdentityBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var viewEvents: IdentityViewEvents

    private val viewModel: IdentityViewModel by activityViewModels {
        InjectorUtils.provideIdentityViewModelFactory(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentIdentityBinding.inflate(inflater, container, false)
        viewModel.attachEvents(this)
        return binding.root
    }

    fun initEvents(viewEvents: IdentityViewEvents) {
        this.viewEvents = viewEvents
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewState()
        initViewEffects()
        initClickListeners()
    }

    private fun initViewState() {
        viewModel.state.address.observe(
            viewLifecycleOwner,
            Observer { address ->
                binding.bchAddress.text = address
                val qrCode = generateQrCode(address)
                binding.bchAddrRqCode.setImageBitmap(qrCode)
            }
        )
    }

    private fun initClickListeners() {
        binding.bchAddrRqCode.setOnClickListener {
            val address = viewModel.state.address.value ?: throw Exception("Address missing!")
            viewEvents.addressClicked(IdentityViewEventType.AddressClicked(address))
        }

        binding.bchAddress.setOnClickListener {
            val address = viewModel.state.address.value ?: throw Exception("Address missing!")
            viewEvents.addressClicked(IdentityViewEventType.AddressClicked(address))
        }

        binding.layoutShareIdentity.setOnClickListener {
            val address = viewModel.state.address.value ?: throw Exception("Address missing!")
            viewEvents.shareAddressClicked(IdentityViewEventType.ShareAddressClicked(address))
        }
    }

    private fun initViewEffects() {
        viewModel.effect.copyAddressToClipBoard.observe(
            viewLifecycleOwner,
            Observer {
                copyAddressToClipBoard(it.address)
            }
        )

        viewModel.effect.shareAddress.observe(
            viewLifecycleOwner,
            Observer {
                shareAddress(it.address)
            }
        )
    }

    private fun copyAddressToClipBoard(address: String) {
        val clipboardManager = activity?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("text", address)
        clipboardManager.setPrimaryClip(clipData)
        Toast.makeText(activity, "Address copied to clipboard", Toast.LENGTH_LONG).show()
    }

    private fun generateQrCode(address: String): Bitmap {
        val writer = QRCodeWriter()
        val bitMatrix = writer.encode(address, BarcodeFormat.QR_CODE, 512, 512)
        val width = bitMatrix.width
        val height = bitMatrix.height
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        for (x in 0 until width) {
            for (y in 0 until height) {
                bitmap.setPixel(x, y, if (bitMatrix.get(x, y)) Color.BLACK else Color.WHITE)
            }
        }
        return bitmap
    }

    private fun shareAddress(address: String) {
        val sendIntent: Intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_TEXT, address)
            type = "text/plain"
        }

        val shareIntent = Intent.createChooser(sendIntent, null)
        startActivity(shareIntent)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
