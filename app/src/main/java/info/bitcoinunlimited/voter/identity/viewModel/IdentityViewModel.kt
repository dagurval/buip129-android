package info.bitcoinunlimited.voter.identity.viewModel

import androidx.lifecycle.ViewModel
import info.bitcoinunlimited.voter.auth.AuthRepository
import info.bitcoinunlimited.voter.identity.view.IdentityFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class IdentityViewModel(
    private val authRepository: AuthRepository
) : ViewModel(), IdentityViewEvents {
    private val _state = _IdentityViewState()
    val state = IdentityViewState(_state)
    private val _effect = _IdentityViewEffect()
    val effect = IdentityViewEffect(_effect)

    init {
        _state._address.value = authRepository.getCurrentUserId()
    }

    override fun attachEvents(fragment: IdentityFragment) {
        fragment.initEvents(this)
    }

    override fun addressClicked(event: IdentityViewEventType.AddressClicked) {
        val address = event.address
        _effect._copyAddressToClipBoard.value = IdentityViewEffectType.CopyAddressToClipBoardEffect(address)
    }

    override fun shareAddressClicked(event: IdentityViewEventType.ShareAddressClicked) {
        val address = event.address
        _effect._shareAddress.value = IdentityViewEffectType.ShareAddressEffect(address)
    }
}
