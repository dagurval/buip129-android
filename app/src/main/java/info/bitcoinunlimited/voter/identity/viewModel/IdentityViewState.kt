package info.bitcoinunlimited.voter.identity.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import info.bitcoinunlimited.voter.identity.view.IdentityFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

/** View state data for identity fragment */
data class _IdentityViewState(
    val _address: MutableLiveData<String> = MutableLiveData(),
    val _balance: MutableLiveData<Double> = MutableLiveData(0.0)
)

data class IdentityViewState(private val _state: _IdentityViewState) {
    val address: LiveData<String> = _state._address
    val balance: LiveData<Double> = _state._balance
}

/** View events for identity fragment **/
@ExperimentalCoroutinesApi
@InternalCoroutinesApi
interface IdentityViewEvents {
    fun attachEvents(fragment: IdentityFragment)
    fun addressClicked(event: IdentityViewEventType.AddressClicked)
    fun shareAddressClicked(event: IdentityViewEventType.ShareAddressClicked)
}

sealed class IdentityViewEventType {
    data class AddressClicked(val address: String) : IdentityViewEventType()
    data class ShareAddressClicked(val address: String) : IdentityViewEventType()
}

/** View effects for identity fragment **/
class _IdentityViewEffect(
    val _copyAddressToClipBoard: MutableLiveData<IdentityViewEffectType.CopyAddressToClipBoardEffect> = MutableLiveData(),
    val _shareAddress: MutableLiveData<IdentityViewEffectType.ShareAddressEffect> = MutableLiveData()
)

class IdentityViewEffect(_effect: _IdentityViewEffect) {
    val copyAddressToClipBoard: LiveData<IdentityViewEffectType.CopyAddressToClipBoardEffect> = _effect._copyAddressToClipBoard
    val shareAddress: LiveData<IdentityViewEffectType.ShareAddressEffect> = _effect._shareAddress
}

sealed class IdentityViewEffectType {
    data class CopyAddressToClipBoardEffect(val address: String) : IdentityViewEffectType()
    data class ShareAddressEffect(val address: String) : IdentityViewEffectType()
}
