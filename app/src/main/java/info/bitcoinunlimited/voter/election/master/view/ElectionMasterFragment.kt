package info.bitcoinunlimited.voter.election.master.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import info.bitcoinunlimited.voter.InjectorUtils
import info.bitcoinunlimited.voter.R.anim.fade_in
import info.bitcoinunlimited.voter.WalletRepository
import info.bitcoinunlimited.voter.auth.viewModel.AuthEvents
import info.bitcoinunlimited.voter.auth.viewModel.AuthViewModel
import info.bitcoinunlimited.voter.databinding.FragmentElectionMasterBinding
import info.bitcoinunlimited.voter.election.master.viewModel.ElectionMasterViewModel
import info.bitcoinunlimited.voter.election.master.viewModel.FeedViewEvent
import info.bitcoinunlimited.voter.election.master.viewModel.FeedViewEventType
import info.bitcoinunlimited.voter.utils.CONTENT_RECYCLER_VIEW_POSITION
import kotlinx.android.synthetic.main.fragment_election_master.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ElectionMasterFragment : Fragment() {
    private lateinit var authEvents: AuthEvents
    private lateinit var binding: FragmentElectionMasterBinding
    private lateinit var adapter: ElectionMasterAdapter
    private val authViewModel: AuthViewModel by viewModels {
        InjectorUtils.provideAuthViewModelFactory(this)
    }
    private var savedRecyclerPosition: Int = 0
    private lateinit var viewEvent: FeedViewEvent

    private val electionViewModel: ElectionMasterViewModel by activityViewModels {
        InjectorUtils.provideElectionMasterViewModelFactory(this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        if (electionRecyclerView != null) outState.putInt(
            CONTENT_RECYCLER_VIEW_POSITION,
            (electionRecyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
        )
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null)
            savedRecyclerPosition = savedInstanceState.getInt(CONTENT_RECYCLER_VIEW_POSITION)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initFeed()
    }

    private fun initFeed() = GlobalScope.launch(Dispatchers.Default) {
        val walletRepository = WalletRepository.getInstance()
        val authRepository = InjectorUtils.getAuthRepository()

        walletRepository.initWallet(activity?.applicationContext ?: throw Exception("Cannot get applicationContext"))
        val challenge = authRepository.requestChallenge()
        val payDestination = WalletRepository.getInstance().getIdentity()
        val signedChallenge = authRepository.signChallenge(payDestination, challenge)
        val jsonWebToken = authRepository.identify(signedChallenge)
        authRepository.signInWithCustomJwtAndLinkAuths(jsonWebToken)
        electionViewModel.getFeed(FeedViewEventType.FeedLoad)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentElectionMasterBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewmodel = this.electionViewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        attachEvents()
        initAdapter()
        observeViewState()
        initSwipeToRefresh()
        initViewEffects()
        electionViewModel.getFeed(FeedViewEventType.FeedLoad)
    }

    private fun initViewEffects() {
        electionViewModel.effect.screenEmpty.observe(viewLifecycleOwner) {
            val emptyFeedView = binding.emptyFeedLayout.emptyFeedView
            if (!it.isEmpty)
                emptyFeedView.visibility = View.GONE
            else {
                if (emptyFeedView.visibility == View.GONE) {
                    val fadeIn = AnimationUtils.loadAnimation(context, fade_in)
                    emptyFeedView.startAnimation(fadeIn)
                    fadeIn.setAnimationListener(object : Animation.AnimationListener {
                        override fun onAnimationRepeat(animation: Animation?) { /*Do something.*/
                        }

                        override fun onAnimationEnd(animation: Animation?) {
                            emptyFeedView.visibility = VISIBLE
                            binding.electionRecyclerView.visibility = VISIBLE
                        }

                        override fun onAnimationStart(animation: Animation?) {
                            binding.electionRecyclerView.visibility = INVISIBLE
                        }
                    })
                }
            }
        }
    }

    fun initEvents(viewEvents: FeedViewEvent) {
        this.viewEvent = viewEvents
    }

    fun initAuthEvents(viewEvents: AuthEvents) {
        this.authEvents = viewEvents
    }

    private fun attachEvents() {
        electionViewModel.attachEvents(this)
        authViewModel.attachEvents(this)
    }

    private fun initAdapter() {
        adapter =
            ElectionMasterAdapter(
                electionViewModel,
                this.viewEvent
            )

        electionRecyclerView.layoutManager = LinearLayoutManager(context)
        electionRecyclerView.adapter = adapter
    }
    private fun observeViewState() {
        this.electionViewModel.state.electionList.observe(
            viewLifecycleOwner,
            Observer { pagedList ->
                adapter.submitList(pagedList)
                this.viewEvent.feedLoadComplete(FeedViewEventType.FeedLoadComplete(pagedList.isNotEmpty()))
            }
        )
    }

    private fun initSwipeToRefresh() {
        this.electionViewModel.state.isRefreshing.observe(
            viewLifecycleOwner,
            Observer { isRefreshing: Boolean ->
                swipeToRefresh.isRefreshing = isRefreshing
            }
        )
        swipeToRefresh.setOnRefreshListener {
            this.viewEvent.swipeToRefresh(FeedViewEventType.SwipeToRefresh)
        }
    }
}
