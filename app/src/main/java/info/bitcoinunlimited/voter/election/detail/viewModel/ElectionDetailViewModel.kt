package info.bitcoinunlimited.voter.election.detail.viewModel

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import bitcoinunlimited.libbitcoincash.BCHinput
import bitcoinunlimited.libbitcoincash.BCHscript
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.TwoOptionVoteContract
import info.bitcoinunlimited.voter.ElectrumAPI
import info.bitcoinunlimited.voter.WalletRepository
import info.bitcoinunlimited.voter.election.detail.view.ElectionDetailFragment
import info.bitcoinunlimited.voter.election.getOrCreateContractCoin
import info.bitcoinunlimited.voter.election.room.Election
import info.bitcoinunlimited.voter.utils.TAG_VOTE
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class ElectionDetailViewModel(
    private val walletRepository: WalletRepository,
    private val electrumAPI: ElectrumAPI
) : ViewModel(), ElectionDetailEvents {
    private val _state = _ElectionDetailViewState()
    val state = ElectionDetailViewState(_state)

    fun setElection(election: Election) = GlobalScope.launch(Dispatchers.Main) {
        _state._election.value = election
    }

    /** View events */
    fun attachEvents(fragment: Fragment) {
        when (fragment) {
            is ElectionDetailFragment -> fragment.initEvents(this)
        }
    }

    private suspend fun castVote(chain: ChainSelector, identity: PayDestination) = viewModelScope.launch(Dispatchers.IO) {
        val contract = walletRepository.createContract(identity)
        val coin = try {
            getOrCreateContractCoin(electrumAPI, chain, identity, contract)
        } catch (e: Exception) {
            // TODO: Show error un UI that casting vote failed
            Log.e(TAG_VOTE, "Casting vote failed: $e")
            return@launch
        }
        val castInput = BCHinput(chain, coin, BCHscript(chain))
        val castTx = contract.castVote(
            chain, castInput, identity,
            TwoOptionVoteContract.BLANK_VOTE
        )
        val castTxID = try {
            electrumAPI.broadcast(castTx)
        } catch (e: Exception) {
            // TODO: Show error in UI that casting vote failed
            Log.e(TAG_VOTE, "Casting vote failed: $e")
            return@launch
        }
        Log.i(TAG_VOTE, "Contract vote cast in tx $castTxID")
    }

    override fun voteSubmitted(event: ElectionDetailEvent.VoteSubmitted) = viewModelScope.launch {
        castVote(ChainSelector.BCHMAINNET, walletRepository.getIdentity())
    }

    class ElectionDetailViewModelFactory(
        private val walletRepository: WalletRepository,
        private val electrumAPI: ElectrumAPI
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(ElectionDetailViewModel::class.java))
                return ElectionDetailViewModel(walletRepository, electrumAPI) as T
            else
                throw IllegalArgumentException("ViewModel class must be a subclass of ElectionDetailViewModel!")
        }
    }
}
