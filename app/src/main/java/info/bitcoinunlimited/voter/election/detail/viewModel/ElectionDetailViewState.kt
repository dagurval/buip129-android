package info.bitcoinunlimited.voter.election.detail.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import info.bitcoinunlimited.voter.election.room.Election

/** View state data for single election */
data class _ElectionDetailViewState(
    val _election: MutableLiveData<Election> = MutableLiveData()
)

data class ElectionDetailViewState(val _state: _ElectionDetailViewState) {
    val election: LiveData<Election> = _state._election
}
