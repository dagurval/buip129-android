package info.bitcoinunlimited.voter.election.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Election::class], version = 3, exportSchema = false)
abstract class ElectionDatabase : RoomDatabase() {
    abstract fun electionDao(): ElectionDao

    companion object {
        // For Singleton instantiation
        @Volatile private var instance: ElectionDatabase? = null

        fun getInstance(context: Context): ElectionDatabase {
            return instance
                ?: synchronized(this) {
                    instance
                        ?: buildDatabase(
                            context
                        )
                            .also { instance = it }
                }
        }

        private fun buildDatabase(context: Context): ElectionDatabase {
            return Room.databaseBuilder(
                context, ElectionDatabase::class.java,
                "election-db"
            ).fallbackToDestructiveMigration().build()
        }
    }
}
