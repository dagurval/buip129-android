package info.bitcoinunlimited.voter.election.master.view

import android.view.LayoutInflater
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import info.bitcoinunlimited.voter.databinding.ItemElectionBinding.inflate
import info.bitcoinunlimited.voter.election.master.viewModel.ElectionMasterViewModel
import info.bitcoinunlimited.voter.election.master.viewModel.FeedViewEvent
import info.bitcoinunlimited.voter.election.room.Election
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Election>() {
    override fun areItemsTheSame(oldElection: Election, newElection: Election): Boolean =
        oldElection.id == newElection.id

    override fun areContentsTheSame(oldElection: Election, newElection: Election): Boolean =
        oldElection == newElection
}

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ElectionMasterAdapter(
    private val electionViewModel: ElectionMasterViewModel,
    val viewEvents: FeedViewEvent
) :
    ListAdapter<Election, ElectionMasterAdapter.ViewHolder>(
        DIFF_CALLBACK
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(
            inflate(LayoutInflater.from(parent.context), parent, false).apply {
                this.viewmodel = electionViewModel
            }
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val election = getItem(position) ?: return
        holder.bind(createOnClickListener(election, position, holder), election)
    }

    private fun createOnClickListener(election: Election, position: Int, holder: ViewHolder) = OnClickListener { _ ->
        val navigateToElectionDetailAction =
            ElectionMasterFragmentDirections.actionElectionMasterFragmentToElectionDetailFragment(
                election.title ?: election.id,
                election
            )
        holder.itemView.findNavController().navigate(navigateToElectionDetailAction)
    }

    class ViewHolder(private var binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(onClickListener: OnClickListener, data: Any) {
            binding.setVariable(BR.data, data)
            binding.setVariable(BR.clickListener, onClickListener)
        }
    }
}
