package info.bitcoinunlimited.voter.election.room

import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ElectionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(elections: List<Election>)

    @Query("SELECT * FROM election_table ORDER BY created_at DESC")
    fun getMainFeedRoom(): DataSource.Factory<Int, Election>
}
