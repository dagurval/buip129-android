package info.bitcoinunlimited.voter.election

import androidx.lifecycle.asFlow
import androidx.paging.Config
import androidx.paging.PagedList
import androidx.paging.toLiveData
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import info.bitcoinunlimited.voter.election.room.Election
import info.bitcoinunlimited.voter.election.room.ElectionDao
import info.bitcoinunlimited.voter.utils.Resource
import info.bitcoinunlimited.voter.utils.Resource.Companion.loading
import info.bitcoinunlimited.voter.utils.Resource.Companion.success
import info.bitcoinunlimited.voter.utils.awaitRealtime
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.tasks.await

class ElectionRepository(private val electionDao: ElectionDao) {

    fun getElectionsNetwork(current_user_id: String) = flow<Resource<Flow<PagedList<Election>>>> {
        emit(loading(null))
        // val labeledSet = HashSet<String>()
        // syncLabeledContent(labeledSet, this)
        getLoggedInNonRealtimeContent(current_user_id, this)
    }

    private suspend fun getLoggedInNonRealtimeContent(
        current_user_id: String,
        flow: FlowCollector<Resource<Flow<PagedList<Election>>>>
    ) {
        try {
            val electionSnapshotList = Firebase.firestore.collection("election")
                .whereArrayContains("participantAddresses", current_user_id)
                .get()
                .await()

            val electionList = electionSnapshotList.map { electionSnap ->
                Election(
                    id = electionSnap.id,
                    title = electionSnap.get("title") as String? ?: "**Title Missing**",
                    description = electionSnap.get("description") as String?,
                    adminAddress = electionSnap.get("adminAddress") as String?,
                    salt = electionSnap.get("salt") as String?,
                    createdAt = electionSnap.getTimestamp("createdAt")!!.seconds as Long,
                    modifiedAt = electionSnap.getTimestamp("modifiedAt")!!.seconds as Long
                )
            }

            electionDao.insert(electionList)
            val elections = getElectionsRoom()
            flow.emit(success(elections))
        } catch (error: FirebaseFirestoreException) {
            val error = Resource.error("CONTENT_LOGGED_IN_NON_REALTIME_ERROR ${error.localizedMessage}", null)
            flow.emit(error)
        }
    }

    private val pagedListConfig = Config(
        prefetchDistance = 100,
        pageSize = 100
    )

    fun getElectionsRoom() = electionDao.getMainFeedRoom().toLiveData(pagedListConfig).asFlow()

    private suspend fun syncLabeledContent(
        labeledSet: HashSet<String>,
        flow: FlowCollector<Resource<Flow<PagedList<Election>>>>
    ) {
        val response = Firebase.firestore.collection("election").awaitRealtime()
        if (response.error == null) {
            val contentList = response.packet?.documentChanges?.map { doc ->
                doc.document.toObject(Election::class.java).also { content ->
                    labeledSet.add(content.id)
                }
            }
            println(contentList)
            // feedDao.insertFeed(contentList)
        }
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: ElectionRepository? = null

        fun getInstance(electionDao: ElectionDao) =
            instance
                ?: synchronized(this) {
                    instance
                        ?: ElectionRepository(
                            electionDao
                        )
                            .also { instance = it }
                }
    }
}
