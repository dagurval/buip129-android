package info.bitcoinunlimited.voter.election

import android.util.Log
import bitcoinunlimited.libbitcoincash.BCHinput
import bitcoinunlimited.libbitcoincash.BCHscript
import bitcoinunlimited.libbitcoincash.BCHspendable
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.TwoOptionVoteContract
import info.bitcoinunlimited.voter.ElectrumAPI
import info.bitcoinunlimited.voter.utils.TAG_ELECTRUM
import info.bitcoinunlimited.voter.utils.TAG_VOTE

/**
 * Fetches an unused utxo from the contract. If no such utxo exists, creates one by spending coins from identity utxos.
 */
public suspend fun getOrCreateContractCoin(
    electrum: ElectrumAPI,
    chain: ChainSelector,
    identity: PayDestination,
    contract: TwoOptionVoteContract
): BCHspendable {
    // First check if contract has coins
    val contractAddress = contract.address(chain)
    val contractCoins = electrum.listUnspent(contractAddress)
    val dust = 546
    for (c in contractCoins) {
        if (c.amount >= dust + TwoOptionVoteContract.CAST_FEE) {
            c.secret = identity.secret
            return c
        }
    }

    // No coins in contract, so we send a coin to the contract
    var fundCoin: BCHspendable? = null
    val minInput = TwoOptionVoteContract.MIN_CONTRACT_INPUT + TwoOptionVoteContract.FUND_FEE
    for (c in electrum.listUnspent(identity)) {
        if (c.amount >= minInput) {
            fundCoin = c
            break
        }
    }
    if (fundCoin == null) {
        error("Not enough funds to cast a vote")
    }
    val fundInput = BCHinput(chain, fundCoin, BCHscript(chain))
    val fundTx = contract.fundContract(chain, fundInput, identity)
    val txHash = electrum.broadcast(fundTx)
    Log.i(TAG_VOTE, "Contract funded in tx $txHash")
    if (txHash != fundTx.calcHash().toHex()) {
        Log.w(TAG_ELECTRUM, "Expected fund tx hash ${fundTx.calcHash()}, but electrum returned $txHash")
    }

    val index = 0
    val newCoin = BCHspendable(chain, fundTx.calcHash(), index, fundTx.outputs[index].amount)
    newCoin.priorOutScript = fundTx.outputs[0].script
    newCoin.secret = identity.secret
    return newCoin
}

public suspend fun logIdentityCoins(electrumAPI: ElectrumAPI, identity: PayDestination) {
    val coins = electrumAPI.listUnspent(identity)
    Log.i(TAG_ELECTRUM, "Identity ${identity.address!!} has ${coins.size} coins.")
    coins.map {
        Log.i(TAG_ELECTRUM, "Identity owns coin $it")
    }
}
