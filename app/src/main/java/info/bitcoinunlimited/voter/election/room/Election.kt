package info.bitcoinunlimited.voter.election.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable
import java.util.Date

@Entity(tableName = "election_table")
data class Election(
    @PrimaryKey var id: String,
    val description: String?,
    val title: String = "**No title**",
    val adminAddress: String?,
    val salt: String?,
    @ColumnInfo(name = "created_at") val createdAt: Long,
    @ColumnInfo(name = "modified_at") val modifiedAt: Long

) : Serializable {
    // Required by firestore
    constructor() : this("", null, "**No title**", null, null, 1L, 1L)

    val createdDate: Date get() {
        return Date(createdAt * 1000)
    }
}
