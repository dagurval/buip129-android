package info.bitcoinunlimited.voter.election.master.viewModel

import androidx.fragment.app.Fragment
import androidx.lifecycle.* // ktlint-disable no-wildcard-imports
import info.bitcoinunlimited.voter.auth.AuthRepository
import info.bitcoinunlimited.voter.election.ElectionRepository
import info.bitcoinunlimited.voter.election.master.view.ElectionMasterFragment
import info.bitcoinunlimited.voter.utils.Status
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ElectionMasterViewModel(
    private val electionRepository: ElectionRepository,
    private val authRepository: AuthRepository
) : ViewModel(),
    FeedViewEvent {
    private val _state = _FeedViewState()
    val state = FeedViewState(_state)
    private val _effect = _FeedViewEffect()
    val effect = FeedViewEffect(_effect)

    /** View events */
    fun attachEvents(fragment: Fragment) {
        when (fragment) {
            is ElectionMasterFragment -> fragment.initEvents(this)
        }
    }

    fun getFeed(event: FeedViewEventType) = viewModelScope.launch(Dispatchers.IO) {
        val currentUserId = authRepository.getCurrentUserId() ?: throw Error("Cannot get currentUserId")
        electionRepository.getElectionsNetwork(currentUserId).onEach { resource ->
            withContext(Dispatchers.Main) {
                when (resource.status) {
                    Status.LOADING -> {
                        if (event is FeedViewEventType.SwipeToRefresh)
                            _effect._swipeToRefresh.value = FeedViewEffectType.SwipeToRefreshEffect(true)
                        getElectionsLocal()
                    }
                    Status.SUCCESS -> {
                        setSwipeToRefreshState(false)
                        resource.data?.collect { pagedList ->
                            _state._feedList.value = pagedList
                        }
                    }
                    Status.ERROR -> {
                        getElectionsLocal()
                    }
                }
            }
        }.flowOn(Dispatchers.IO).launchIn(viewModelScope)
    }

    private fun getElectionsLocal() {
        electionRepository.getElectionsRoom().onEach { pagedList ->
            withContext(Dispatchers.Main) {
                _state._feedList.value = pagedList
            }
        }.flowOn(Dispatchers.IO).launchIn(viewModelScope)
    }

    private fun setSwipeToRefreshState(isRefreshing: Boolean) {
        _state._isRefreshing.value = isRefreshing
    }

    override fun feedLoadComplete(event: FeedViewEventType.FeedLoadComplete) {
        _effect._screenEmpty.value = FeedViewEffectType.ScreenEmptyEffect(!event.hasContent)
    }

    override fun swipeToRefresh(event: FeedViewEventType.SwipeToRefresh) {
        getFeed(FeedViewEventType.SwipeToRefresh)
    }
}
