package info.bitcoinunlimited.voter.election.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Election::class], version = 1)
abstract class ElectionDatabaseBuilder : RoomDatabase() {

    abstract fun electionDao(): ElectionDao

    companion object {
        private val DATABASE_NAME = "election-db"
        private var INSTANCE: ElectionDatabaseBuilder? = null
        fun getAppDatabase(context: Context): ElectionDatabaseBuilder {
            if (INSTANCE == null)
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    ElectionDatabaseBuilder::class.java, DATABASE_NAME
                )
                    .build()
            return INSTANCE as ElectionDatabaseBuilder
        }
    }
}
