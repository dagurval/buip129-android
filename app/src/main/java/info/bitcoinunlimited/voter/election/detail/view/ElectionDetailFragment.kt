package info.bitcoinunlimited.voter.election.detail.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import info.bitcoinunlimited.voter.InjectorUtils
import info.bitcoinunlimited.voter.databinding.FragmentElectionDetailBinding
import info.bitcoinunlimited.voter.election.detail.viewModel.ElectionDetailEvent
import info.bitcoinunlimited.voter.election.detail.viewModel.ElectionDetailEvents
import info.bitcoinunlimited.voter.election.detail.viewModel.ElectionDetailViewModel
import kotlinx.android.synthetic.main.fragment_election_detail.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class ElectionDetailFragment : Fragment() {
    private lateinit var viewEvents: ElectionDetailEvents
    private lateinit var binding: FragmentElectionDetailBinding
    private val safeArgs: ElectionDetailFragmentArgs by navArgs()
    val viewModel: ElectionDetailViewModel by activityViewModels {
        InjectorUtils.provideElectionDetailViewModelFactory()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.attachEvents(this)
        viewModel.setElection(safeArgs.election)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val safeArgs: ElectionDetailFragmentArgs by navArgs()
        binding = FragmentElectionDetailBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        return binding.root
    }

    fun initEvents(viewEvents: ElectionDetailEvents) {
        this.viewEvents = viewEvents
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClickListeners()
        observeViewState()
        observeViewEffects()
    }

    private fun initClickListeners() {
        electionDetailSubmitVote.setOnClickListener {
            viewEvents.voteSubmitted(ElectionDetailEvent.VoteSubmitted)
        }
    }

    private fun observeViewState() {
    }

    private fun observeViewEffects() {
    }
}
