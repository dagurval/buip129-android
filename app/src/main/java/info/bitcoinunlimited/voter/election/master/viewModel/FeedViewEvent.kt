package info.bitcoinunlimited.voter.election.master.viewModel

import info.bitcoinunlimited.voter.election.master.viewModel.FeedViewEventType.* // ktlint-disable no-wildcard-imports

/**
 * View state events for content feeds
 */
interface FeedViewEvent {
    fun feedLoadComplete(event: FeedLoadComplete)
    fun swipeToRefresh(event: SwipeToRefresh)
}

sealed class FeedViewEventType {
    object FeedLoad : FeedViewEventType()
    data class FeedLoadComplete(val hasContent: Boolean) : FeedViewEventType()
    object SwipeToRefresh : FeedViewEventType()
}
