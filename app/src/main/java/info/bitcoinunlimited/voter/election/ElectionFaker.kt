package info.bitcoinunlimited.voter.election

import com.github.javafaker.Faker
import info.bitcoinunlimited.voter.election.room.Election
import java.util.UUID

object ElectionFaker {

    private val faker = Faker()

    private fun generateElection(): Election {
        val id = UUID.randomUUID().toString()
        val title = faker.lorem().words(2).reduce { acc, s -> "$acc $s" }
        val description = faker.lorem().words(20).reduce { acc, s -> "$acc $s" }
        val adminId = UUID.randomUUID().toString()
        val salt = UUID.randomUUID().toString()

        return Election(id, description, description, adminId, salt, System.currentTimeMillis(), System.currentTimeMillis())
    }

    fun generateElections(amount: Int): List<Election> {
        val elections = mutableListOf<Election>()
        for (x in 0 until amount) {
            val election =
                generateElection()
            elections.add(election)
        }

        return elections.toList()
    }
}
