package info.bitcoinunlimited.voter.election.master.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import info.bitcoinunlimited.voter.election.room.Election

/** View state data for content feeds */
data class _FeedViewState(
    val _feedList: MutableLiveData<PagedList<Election>> = MutableLiveData(),
    val _isRefreshing: MutableLiveData<Boolean> = MutableLiveData(false)
)

data class FeedViewState(private val _state: _FeedViewState) {
    val electionList: LiveData<PagedList<Election>> = _state._feedList
    val isRefreshing: LiveData<Boolean> = _state._isRefreshing
}
