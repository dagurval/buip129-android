package info.bitcoinunlimited.voter.election.master.viewModel

import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import info.bitcoinunlimited.voter.auth.AuthRepository
import info.bitcoinunlimited.voter.election.ElectionRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
class ElectionMasterViewModelFactory(
    owner: SavedStateRegistryOwner,
    private val electionRepository: ElectionRepository,
    private val authRepository: AuthRepository
) : AbstractSavedStateViewModelFactory(owner, null) {
    override fun <T : ViewModel?> create(key: String, modelClass: Class<T>, state: SavedStateHandle) =
        ElectionMasterViewModel(electionRepository, authRepository) as T
}
