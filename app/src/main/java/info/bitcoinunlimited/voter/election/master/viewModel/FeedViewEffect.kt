package info.bitcoinunlimited.voter.election.master.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import info.bitcoinunlimited.voter.election.master.viewModel.FeedViewEffectType.* // ktlint-disable no-wildcard-imports

/** View state effects for content feeds */
class _FeedViewEffect(
    val _signIn: MutableLiveData<SignInEffect> = MutableLiveData(),
    val _notifyItemChanged: MutableLiveData<NotifyItemChangedEffect> = MutableLiveData(),
    val _contentLoadingIds: HashSet<String> = hashSetOf(),
    val _enableSwipeToRefresh: MutableLiveData<EnableSwipeToRefreshEffect> = MutableLiveData(),
    val _swipeToRefresh: MutableLiveData<SwipeToRefreshEffect> = MutableLiveData(),
    val _contentSwiped: MutableLiveData<ContentSwipedEffect> = MutableLiveData(),
    val _snackBar: MutableLiveData<SnackBarEffect> = MutableLiveData(),
    val _openContentSourceIntent: MutableLiveData<OpenContentSourceIntentEffect> = MutableLiveData(),
    val _screenEmpty: MutableLiveData<ScreenEmptyEffect> = MutableLiveData()
)

class FeedViewEffect(_effect: _FeedViewEffect) {
    val signIn: LiveData<SignInEffect> = _effect._signIn
    val notifyItemChanged: LiveData<NotifyItemChangedEffect> = _effect._notifyItemChanged
    val contentLoadingIds: HashSet<String> = _effect._contentLoadingIds
    val enableSwipeToRefresh: LiveData<EnableSwipeToRefreshEffect> = _effect._enableSwipeToRefresh
    val swipeToRefresh: LiveData<SwipeToRefreshEffect> = _effect._swipeToRefresh
    val contentSwiped: LiveData<ContentSwipedEffect> = _effect._contentSwiped
    val snackBar: LiveData<SnackBarEffect> = _effect._snackBar
    val openContentSourceIntent: LiveData<OpenContentSourceIntentEffect> = _effect._openContentSourceIntent
    val screenEmpty: LiveData<ScreenEmptyEffect> = _effect._screenEmpty
}

sealed class FeedViewEffectType {
    data class SignInEffect(val toSignIn: Boolean) : FeedViewEffectType()
    data class NotifyItemChangedEffect(val position: Int) : FeedViewEffectType()
    data class EnableSwipeToRefreshEffect(val isEnabled: Boolean) : FeedViewEffectType()
    data class SwipeToRefreshEffect(val isEnabled: Boolean) : FeedViewEffectType()
    data class ContentSwipedEffect(val position: Int) : FeedViewEffectType()
    data class SnackBarEffect(val text: String) : FeedViewEffectType()
    data class OpenContentSourceIntentEffect(val url: String) : FeedViewEffectType()
    data class ScreenEmptyEffect(val isEmpty: Boolean) : FeedViewEffectType()
}
