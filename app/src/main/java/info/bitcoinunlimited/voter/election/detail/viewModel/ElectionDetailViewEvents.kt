package info.bitcoinunlimited.voter.election.detail.viewModel

import info.bitcoinunlimited.voter.election.detail.viewModel.ElectionDetailEvent.VoteSubmitted
import kotlinx.coroutines.Job

interface ElectionDetailEvents {
    fun voteSubmitted(event: VoteSubmitted): Job
}

sealed class ElectionDetailEvent {
    object VoteSubmitted : ElectionDetailEvent()
}
