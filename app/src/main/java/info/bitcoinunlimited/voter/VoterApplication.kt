package info.bitcoinunlimited.voter

import android.app.Application
import android.util.Log
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Initialize
import com.google.firebase.functions.FirebaseFunctions
import info.bitcoinunlimited.voter.auth.model.Challenge
import info.bitcoinunlimited.voter.utils.TAG_ELECTRUM
import java.lang.Exception
import java.security.PublicKey
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class VoterApplication : Application() {
    private val authRepository = InjectorUtils.getAuthRepository()

    private lateinit var electrumAPI: ElectrumAPI

    companion object {
        init {
            System.loadLibrary("bitcoincashandroid")
            Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
        }
    }

    override fun onCreate() {
        super.onCreate()
        @Suppress("ConstantConditionIf")
        if (BuildConfig.BUILD_TYPE == "emulator")
            useFirebaseEmulators()

        authRepository.observeAuth()

        electrumAPI = ElectrumAPI.getInstance(ChainSelector.BCHMAINNET)
        GlobalScope.launch(Dispatchers.IO) {
            try {
                // First call to electrum API initializes the connection.
                electrumAPI.ping()
                // logIdentityCoins(electrumAPI, walletRepository.getIdentity())
            } catch (e: Exception) {
                Log.e(TAG_ELECTRUM, "Unexpected error pinging server: $e")
            }
        }
    }

    private fun useFirebaseEmulators() {
        // 10.0.2.2 is the special IP address to connect to the 'localhost' of
        // the host computer from an Android emulator.
//        val settings = FirebaseFirestoreSettings.Builder()
//            .setHost("10.0.2.2:8080")
//            .setSslEnabled(false)
//            .setPersistenceEnabled(false)
//            .build()
//
//        val firestore = FirebaseFirestore.getInstance()
//        firestore.firestoreSettings = settings

        FirebaseFunctions.getInstance().useFunctionsEmulator("http://10.0.2.2:5001")
    }

    private suspend fun verifyChallenge(publicKey: PublicKey) {
        // val challenge = AuthRepository.requestChallenge() ?: return
        val challengeMock = Challenge("challengeMock")
        // val signedChallenge = CryptoRepository.sign(challengeMock) ?: return
        // val result = AuthRepository.verifySignedChallenge(publicKey, signedChallenge) ?: return
        // AuthRepository.signInWithCustomJwtAndLinkAuths(result)
    }
}
