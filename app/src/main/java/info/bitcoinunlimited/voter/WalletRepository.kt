package info.bitcoinunlimited.voter

import android.content.Context
import bitcoinunlimited.libbitcoincash.Bip44Wallet
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.Hash
import bitcoinunlimited.libbitcoincash.OpenKvpDB
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.PlatformContext
import bitcoinunlimited.libbitcoincash.TwoOptionVote
import bitcoinunlimited.libbitcoincash.TwoOptionVoteContract
import bitcoinunlimited.libbitcoincash.walletDb
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

@ExperimentalCoroutinesApi
class WalletRepository {
    private val _wallet: MutableStateFlow<Bip44Wallet?> = MutableStateFlow(null) // private mutable state flow
    val wallet: StateFlow<Bip44Wallet?> get() = _wallet // publicly exposed as read-only state flow
    val secretWordList = "trade box today light need route design birth turn insane oxygen sense"

    fun initWallet(context: Context) = runBlocking(Dispatchers.IO) {
        if (wallet.value != null) {
            return@runBlocking
        }
        val context = PlatformContext(context)
        val dbPrefix = "voter.cash"
        walletDb = OpenKvpDB(context, dbPrefix + "bip44walletdb")
        _wallet.value = Bip44Wallet(
            "my-wallet",
            ChainSelector.BCHMAINNET,
            secretWordList
        )
    }

    suspend fun getIdentity() = suspendCoroutine<PayDestination> { continuation ->
        if (wallet.value !== null) {
            val identity = wallet.value?.getDestinationAtIndex(0) ?: throw Exception("Cannot get wallet")
            continuation.resume(identity)
        } else {
            GlobalScope.launch(Dispatchers.IO) {
                wallet.collect { newWallet ->
                    if (newWallet != null) {
                        val identity = newWallet.getDestinationAtIndex(0)
                        continuation.resume(identity)
                        this.cancel()
                    }
                }
            }
        }
    }

    fun createContract(identity: PayDestination): TwoOptionVoteContract {
        val salt = "unittest".toByteArray()
        val description = "Foo?"
        val optA = "Bar"
        val optB = "Baz"
        val optAHash = TwoOptionVote.hash160_salted(salt, optA.toByteArray())
        val optBHash = TwoOptionVote.hash160_salted(salt, optB.toByteArray())
        val endheight = 1000000
        // We're the only one participating in this vote
        val participants = arrayOf(
            Hash.hash160(identity.pubkey!!)
        )
        val proposalID = TwoOptionVote.calculate_proposal_id(
            salt, description, optA, optB, endheight, participants
        )

        return TwoOptionVoteContract(proposalID, optAHash, optBHash, participants[0])
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: WalletRepository? = null

        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: WalletRepository().also { instance = it }
            }
    }
}
