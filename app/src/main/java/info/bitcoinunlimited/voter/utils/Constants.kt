package info.bitcoinunlimited.voter.utils

import info.bitcoinunlimited.voter.BuildConfig

const val CONTENT_RECYCLER_VIEW_POSITION = "contentRecyclerViewPosition"
const val ADAPTER_POSITION_KEY = 122218133
const val CONTENT_REQUEST_NETWORK_ERROR = "Unable to update feed. Please try again later!"
const val CONTENT_REQUEST_SWIPE_TO_REFRESH_ERROR = "Unable to update feed. Swipe to refresh to try again!"

/**
 * Constants for voter.cash
 * @Firebase
 * Firebase-related paths
 * @Functions
 * Serverless node-js endpoints
 */
object Constants {
    //
    object Firebase {
        object Functions {
            const val RequestChallenge = "request_challenge"
            const val VerifySignedChallenge = "verifySignedChallenge"
            const val Signature = "signature"
            const val PublicKey = "publicKey"
            const val JsonWebToken = "jsonWebToken"
            const val Challenge = "challenge"
        }
    }

    val EndpointUrl: String = when (BuildConfig.BUILD_TYPE) {
        "emulator" -> "http://10.0.2.2:5001/voter-6700b/europe-west1/"
        else -> "https://europe-west1-voter-6700b.cloudfunctions.net/"
    }
}
