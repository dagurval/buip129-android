package info.bitcoinunlimited.voter

import android.util.Log
import bitcoinunlimited.libbitcoincash.BCHoutpoint
import bitcoinunlimited.libbitcoincash.BCHscript
import bitcoinunlimited.libbitcoincash.BCHserialized
import bitcoinunlimited.libbitcoincash.BCHspendable
import bitcoinunlimited.libbitcoincash.BCHtransaction
import bitcoinunlimited.libbitcoincash.ChainSelector
import bitcoinunlimited.libbitcoincash.ElectrumClient
import bitcoinunlimited.libbitcoincash.ElectrumRequestTimeout
import bitcoinunlimited.libbitcoincash.Hash256
import bitcoinunlimited.libbitcoincash.PayAddress
import bitcoinunlimited.libbitcoincash.PayDestination
import bitcoinunlimited.libbitcoincash.SerializationType
import info.bitcoinunlimited.voter.utils.TAG_ELECTRUM
import java.util.Timer
import java.util.TimerTask
import kotlin.concurrent.scheduleAtFixedRate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class ElectrumAPI(val chain: ChainSelector) {
    private var cli: ElectrumClient? = null
    private var cliMutex = Mutex()

    /**
     * Task for sending ping request to the server at 1 minute interval.
     */
    private var pingTask: TimerTask? = null

    private val ELECTRUM_SERVERS = arrayOf(
        arrayOf("bitcoincash.network", 50001),
        arrayOf("electroncash.de", 50001),
        arrayOf("bch.imaginary.cash", 50001),
        arrayOf("electroncash.dk", 50001),
        arrayOf("electrum.imaginary.cash", 50001)
    )

    // requires cliMutex lock
    private suspend fun connect() {

        if (cli != null) {
            return
        }
        if (chain != ChainSelector.BCHMAINNET) {
            error("NYI. Only BCH mainnet is supported.")
        }

        pingTask = null

        for (server in ELECTRUM_SERVERS) {
            val host = server[0] as String
            val port = server[1] as Int
            try {
                Log.i(TAG_ELECTRUM, "Connecting to $host:$port ...")
                cli = ElectrumClient(chain, host, port)
                cli!!.start()
                val version = cli!!.version().toString()
                Log.i(TAG_ELECTRUM, "Connected to $host:$port. Server version $version")
                break
            } catch (e: java.net.SocketTimeoutException) {
                Log.w(TAG_ELECTRUM, "Electrum timeout: $e")
                continue
            } catch (e: java.net.ConnectException) {
                Log.w(TAG_ELECTRUM, "Electrum connection error: $e")
                continue
            } catch (e: ElectrumRequestTimeout) {
                Log.w(TAG_ELECTRUM, "Connected to $host:$port, but server did not respond to version request. Disconnecting.")
                cli!!.stop()
                cli = null
            }
        }

        if (cli == null) {
            error("Failed to connect to electrum servers.")
        }

        // Keep connection alive sending ping request at 1 minute interval.
        pingTask = Timer("electrum ping", true).scheduleAtFixedRate(100, 60 * 1000) {
            GlobalScope.launch(Dispatchers.IO) { this@ElectrumAPI.ping() }
        }
    }

    /**
     * Calls a electrum client method. Connects if disconnected.
     */
    private suspend fun <T> withCli(action: suspend (ElectrumClient) -> T): T {
        cliMutex.withLock {
            for (attempt in 1..10) {
                connect()
                try {
                    return action(cli!!)
                } catch (e: ElectrumRequestTimeout) {
                    Log.w(TAG_ELECTRUM, "Electrum request attempt #$attempt timed out. Re-connecting.")
                    cli!!.stop()
                    cli = null
                }
            }
            error("Failed to submit request to electrum server")
        }
    }

    /**
     * Get server version
     */
    suspend fun version(): Pair<String, String> {
        return withCli {
            return@withCli it.version()
        }
    }

    /**
     * Get spendable coins for this destination.
     *
     * The secret in destination object will be passed on to the
     * BCHspendable instances.
     */
    suspend fun listUnspent(destination: PayDestination): List<BCHspendable> {
        return withCli {
            return@withCli it.listUnspent(destination)
        }
    }

    /**
     * Gets spendable coins for this destination.
     *
     * The BCHspendable instances will not contain the private key to spend the coins.
     */
    suspend fun listUnspent(address: PayAddress): List<BCHspendable> {
        return withCli {
            return@withCli it.listUnspent(address)
        }
    }

    /**
     * Broadcast a transaction
     */
    suspend fun broadcast(tx: BCHtransaction): String {
        return withCli {
            return@withCli it.sendTx(tx.BCHserialize(SerializationType.NETWORK).flatten())
        }
    }

    /**
     * Fetch the script for given outpoint.
     */
    suspend fun fetchOutputScript(outpoint: BCHoutpoint): BCHscript {
        return withCli {
            val tx = getTransaction(outpoint.txid)
            return@withCli tx.outputs.get(outpoint.idx).script
        }
    }

    /**
     * Fetch a transaction
     */
    suspend fun getTransaction(txid: Hash256): BCHtransaction {
        return withCli {
            val txBytes = it.getTx(txid)
            val tx = BCHtransaction(chain)
            tx.BCHdeserialize(BCHserialized(txBytes, SerializationType.NETWORK))
            return@withCli tx
        }
    }

    suspend fun ping() {
        return withCli {
            return@withCli it.ping()
        }
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: ElectrumAPI? = null

        fun getInstance(chain: ChainSelector) =
            instance ?: synchronized(this) {
                instance ?: ElectrumAPI(chain).also { instance = it }
            }
    }
}
