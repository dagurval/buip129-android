package info.bitcoinunlimited.voter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.savedstate.SavedStateRegistryOwner
import bitcoinunlimited.libbitcoincash.ChainSelector
import info.bitcoinunlimited.voter.auth.AuthRepository
import info.bitcoinunlimited.voter.auth.viewModel.AuthViewModelFactory
import info.bitcoinunlimited.voter.election.ElectionRepository
import info.bitcoinunlimited.voter.election.detail.viewModel.ElectionDetailViewModel
import info.bitcoinunlimited.voter.election.master.viewModel.ElectionMasterViewModelFactory
import info.bitcoinunlimited.voter.election.room.ElectionDatabase
import info.bitcoinunlimited.voter.identity.viewModel.IdentityViewModelFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
object InjectorUtils {
    fun provideAuthViewModelFactory(owner: SavedStateRegistryOwner): AuthViewModelFactory {
        val chainSelector: ChainSelector = ChainSelector.BCHMAINNET
        val authRepository = AuthRepository.getInstance()
        val userRepository = getUserRepository()
        val electrumRepository = getElectrumRepository(chainSelector)
        val walletRepository = getWalletRepository()
        return AuthViewModelFactory(authRepository, userRepository, electrumRepository, walletRepository, owner)
    }

    fun provideElectionMasterViewModelFactory(fragment: Fragment): ElectionMasterViewModelFactory {
        val context = fragment.requireContext()
        val electionRepository = getElectionRepository(context)
        val authRepository = getAuthRepository()
        return ElectionMasterViewModelFactory(
            fragment,
            electionRepository,
            authRepository
        )
    }

    fun provideElectionDetailViewModelFactory(): ElectionDetailViewModel.ElectionDetailViewModelFactory {
        val chainSelector: ChainSelector = ChainSelector.BCHMAINNET
        val electrumRepository = getElectrumRepository(chainSelector)
        val walletRepository = getWalletRepository()
        return ElectionDetailViewModel.ElectionDetailViewModelFactory(walletRepository, electrumRepository)
    }

    fun provideIdentityViewModelFactory(fragment: Fragment): IdentityViewModelFactory {
        val authRepository = getAuthRepository()
        return IdentityViewModelFactory(fragment, authRepository)
    }

    private fun getElectionRepository(context: Context): ElectionRepository {
        val electionDao = ElectionDatabase.getInstance(context.applicationContext).electionDao()
        return ElectionRepository.getInstance(electionDao)
    }

    private fun getElectrumRepository(chainSelector: ChainSelector): ElectrumAPI {
        return ElectrumAPI.getInstance(chainSelector)
    }

    private fun getWalletRepository(): WalletRepository {
        return WalletRepository.getInstance()
    }

    fun getUserRepository(): UserRepository {
        return UserRepository.getInstance()
    }

    fun getAuthRepository(): AuthRepository {
        return AuthRepository.getInstance()
    }
}
