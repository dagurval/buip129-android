package info.bitcoinunlimited.voter

import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class UserRepository {

    fun updateNotificationToken(userId: String) = GlobalScope.launch(Dispatchers.IO) {
        val result = FirebaseInstanceId.getInstance().instanceId.await()
        val token = result?.token ?: throw Exception("Cannot get token")
        updateNotificationToken(userId, token)
    }

    suspend fun updateNotificationToken(userId: String, token: String) {
        val tokenMap = mutableMapOf<String, Any>(
            "androidNotificationToken" to token
        )
        val currentUserDocument = Firebase.firestore.collection("user").document(userId)
        val userData = currentUserDocument.get()
            .await()
            .data

        if (userData.isNullOrEmpty())
            currentUserDocument.set(tokenMap)
                .addOnFailureListener {
                    println(it)
                }
        else
            currentUserDocument.update(tokenMap)
                .addOnFailureListener {
                    println(it)
                }
    }

    companion object {
        // For Singleton instantiation
        @Volatile
        private var instance: UserRepository? = null

        fun getInstance() =
            instance ?: synchronized(this) {
                instance
                    ?: UserRepository()
                        .also { instance = it }
            }
    }
}
