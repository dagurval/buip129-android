package info.bitcoinunlimited.voter.auth

import com.google.firebase.crashlytics.FirebaseCrashlytics
import io.mockk.mockkStatic
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.extension.ExtendWith

@OptIn(InternalCoroutinesApi::class)
@ExperimentalCoroutinesApi
@ExtendWith(AuthTestExtensions::class)
class AuthViewModelTests @ExperimentalCoroutinesApi constructor(
    private val testDispatcher: TestCoroutineDispatcher
) {
    @BeforeAll
    fun beforeAll() {
        mockkStatic(FirebaseCrashlytics.getInstance()::class)
    }
}
