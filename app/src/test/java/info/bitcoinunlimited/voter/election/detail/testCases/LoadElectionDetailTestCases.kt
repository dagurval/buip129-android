package info.bitcoinunlimited.voter.election.detail.testCases

import info.bitcoinunlimited.voter.election.ElectionFaker
import info.bitcoinunlimited.voter.election.detail.ElectionDetailLoadElectionTest
import java.util.stream.Stream

fun loadElectionDetailTestCases(): Stream<ElectionDetailLoadElectionTest> = Stream.of(
    ElectionDetailLoadElectionTest(
        mockElection = ElectionFaker.generateElections(1).first()
    )
)
