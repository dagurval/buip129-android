package info.bitcoinunlimited.voter.election

import androidx.lifecycle.asFlow
import androidx.lifecycle.liveData
import app.coinverse.utils.asPagedList
import info.bitcoinunlimited.voter.election.room.Election
import info.bitcoinunlimited.voter.utils.MOCK_GET_MAIN_FEED_LIST_ERROR
import info.bitcoinunlimited.voter.utils.Resource
import info.bitcoinunlimited.voter.utils.Status
import kotlinx.coroutines.flow.flow

val mockElectionContent = Election(
    "idMock",
    "1Mock title",
    "Mock election description, this is a mocked description to test the applications behaviour",
    "adminIdMock",
    "transactionIdMock",
    System.currentTimeMillis(),
    System.currentTimeMillis()
)

val mockDbElectionList = listOf(
    mockElectionContent,
    Election(
        "2idMock2",
        "2Mock title",
        "2Mock election description, this is a mocked description to test the applications behaviour",
        "2adminIdMock",
        "2transactionIdMock",
        System.currentTimeMillis(),
        System.currentTimeMillis()
    ),
    Election(
        "3idMock",
        "3Mock title",
        "3Mock election description, this is a mocked description to test the applications behaviour",
        "3adminIdMock",
        "3transactionIdMock",
        System.currentTimeMillis(),
        System.currentTimeMillis()
    ),
    Election(
        "4idMock",
        "4Mock title",
        "4Mock election description, this is a mocked description to test the applications behaviour",
        "4adminIdMock",
        "4transactionIdMock",
        System.currentTimeMillis(),
        System.currentTimeMillis()
    ),
    Election(
        "3idMock",
        "4Mock title",
        "3Mock election description, this is a mocked description to test the applications behaviour",
        "3adminIdMock",
        "3transactionIdMock",
        System.currentTimeMillis(),
        System.currentTimeMillis()
    )
)

fun mockGetMainFeedList(mockElectionList: List<Election>, status: Status) = flow {
    when (status) {
        Status.LOADING -> emit(Resource.loading(null))
        Status.SUCCESS -> emit(Resource.success(mockQueryMainContentListLiveData(mockElectionList)))
        Status.ERROR -> emit(Resource.error(MOCK_GET_MAIN_FEED_LIST_ERROR, null))
    }
}

fun mockQueryMainContentListLiveData(mockFeedList: List<Election>) = liveData {
    emit(mockFeedList.asPagedList())
}.asFlow()
