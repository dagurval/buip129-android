package info.bitcoinunlimited.voter.election.detail

import info.bitcoinunlimited.voter.election.room.Election

data class ElectionDetailLoadElectionTest(
    val mockElection: Election
)
