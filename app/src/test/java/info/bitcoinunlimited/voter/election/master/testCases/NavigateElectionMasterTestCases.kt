package info.bitcoinunlimited.voter.election.master.testCases

import info.bitcoinunlimited.voter.election.master.NavigateElectionMasterTest
import info.bitcoinunlimited.voter.election.mockDbElectionList
import info.bitcoinunlimited.voter.election.mockElectionContent
import info.bitcoinunlimited.voter.utils.Status
import java.util.stream.Stream

fun navigateElectionListTestCases(): Stream<NavigateElectionMasterTest> = Stream.of(
    NavigateElectionMasterTest(
        recyclerViewPosition = 0,
        mockElectionList = mockDbElectionList,
        mockElection = mockElectionContent,
        status = Status.LOADING
    ),
    NavigateElectionMasterTest(
        recyclerViewPosition = 0,
        mockElectionList = listOf(mockElectionContent),
        mockElection = mockElectionContent,
        status = Status.LOADING
    ),
    NavigateElectionMasterTest(
        recyclerViewPosition = 0,
        mockElectionList = listOf(),
        mockElection = mockElectionContent,
        status = Status.LOADING
    ),

    NavigateElectionMasterTest(
        recyclerViewPosition = 0,
        mockElectionList = mockDbElectionList,
        mockElection = mockElectionContent,
        status = Status.SUCCESS
    ),
    NavigateElectionMasterTest(
        recyclerViewPosition = 0,
        mockElectionList = listOf(mockElectionContent),
        mockElection = mockElectionContent,
        status = Status.SUCCESS
    ),
    NavigateElectionMasterTest(
        recyclerViewPosition = 0,
        mockElectionList = listOf(),
        mockElection = mockElectionContent,
        status = Status.SUCCESS
    ),

    NavigateElectionMasterTest(
        recyclerViewPosition = 0,
        mockElectionList = mockDbElectionList,
        mockElection = mockElectionContent,
        status = Status.ERROR
    ),
    NavigateElectionMasterTest(
        recyclerViewPosition = 0,
        mockElectionList = listOf(mockElectionContent),
        mockElection = mockElectionContent,
        status = Status.ERROR
    ),
    NavigateElectionMasterTest(
        recyclerViewPosition = 0,
        mockElectionList = listOf(),
        mockElection = mockElectionContent,
        status = Status.ERROR
    )
)
