package info.bitcoinunlimited.voter.election.master.testCases

import info.bitcoinunlimited.voter.election.ElectionFaker
import info.bitcoinunlimited.voter.election.master.ElectionMasterLoadElectionsTest
import info.bitcoinunlimited.voter.utils.Status
import java.util.stream.Stream

fun loadElectionsIntoMasterTestCases(): Stream<ElectionMasterLoadElectionsTest> = Stream.of(

    ElectionMasterLoadElectionsTest(
        mockElectionList = listOf(),
        status = Status.LOADING
    ),
    ElectionMasterLoadElectionsTest(
        mockElectionList = ElectionFaker.generateElections(10),
        status = Status.LOADING
    ),
    ElectionMasterLoadElectionsTest(
        mockElectionList = ElectionFaker.generateElections(100),
        status = Status.LOADING
    ),
    ElectionMasterLoadElectionsTest(
        mockElectionList = ElectionFaker.generateElections(1000),
        status = Status.LOADING
    ),

    ElectionMasterLoadElectionsTest(
        mockElectionList = listOf(),
        status = Status.SUCCESS
    ),
    ElectionMasterLoadElectionsTest(
        mockElectionList = ElectionFaker.generateElections(10),
        status = Status.SUCCESS
    ),
    ElectionMasterLoadElectionsTest(
        mockElectionList = ElectionFaker.generateElections(100),
        status = Status.SUCCESS
    ),
    ElectionMasterLoadElectionsTest(
        mockElectionList = ElectionFaker.generateElections(1000),
        status = Status.SUCCESS
    ),

    ElectionMasterLoadElectionsTest(
        mockElectionList = listOf(),
        status = Status.ERROR
    ),
    ElectionMasterLoadElectionsTest(
        mockElectionList = ElectionFaker.generateElections(10),
        status = Status.ERROR
    ),
    ElectionMasterLoadElectionsTest(
        mockElectionList = ElectionFaker.generateElections(100),
        status = Status.ERROR
    ),
    ElectionMasterLoadElectionsTest(
        mockElectionList = ElectionFaker.generateElections(1000),
        status = Status.ERROR
    )
)
