package info.bitcoinunlimited.voter.election.master

import info.bitcoinunlimited.voter.election.room.Election
import info.bitcoinunlimited.voter.utils.Status

data class ElectionMasterLoadElectionsTest(
    val mockElectionList: List<Election>,
    val status: Status
)

data class NavigateElectionMasterTest(
    val mockElectionList: List<Election>,
    val mockElection: Election,
    val status: Status,
    val recyclerViewPosition: Int
)
