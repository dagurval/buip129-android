package info.bitcoinunlimited.voter.election.detail

import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.executor.TaskExecutor
import app.coinverse.utils.TEST_COROUTINE_DISPATCHER_NAMESPACE
import info.bitcoinunlimited.voter.election.ElectionRepository
import info.bitcoinunlimited.voter.election.detail.viewModel.ElectionDetailViewModel
import info.bitcoinunlimited.voter.utils.TEST_COROUTINE_DISPATCHER_KEY
import io.mockk.mockkObject
import io.mockk.unmockkAll
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.extension.AfterAllCallback
import org.junit.jupiter.api.extension.AfterEachCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.BeforeEachCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.api.extension.ParameterContext
import org.junit.jupiter.api.extension.ParameterResolver

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
class ElectionDetailTestExtensions :
    BeforeAllCallback,
    AfterAllCallback,
    BeforeEachCallback,
    AfterEachCallback,
    ParameterResolver {
    override fun beforeAll(context: ExtensionContext?) {
        mockkObject(ElectionRepository)
    }

    override fun afterAll(context: ExtensionContext?) {
        unmockkAll()
    }

    override fun beforeEach(context: ExtensionContext?) {
        // Set Coroutine Dispatcher.
        Dispatchers.setMain(
            context?.root
                ?.getStore(TEST_COROUTINE_DISPATCHER_NAMESPACE)
                ?.get(TEST_COROUTINE_DISPATCHER_KEY, TestCoroutineDispatcher::class.java)!!
        )

        // Set LiveData Executor.
        ArchTaskExecutor.getInstance().setDelegate(object : TaskExecutor() {
            override fun executeOnDiskIO(runnable: Runnable) = runnable.run()
            override fun postToMainThread(runnable: Runnable) = runnable.run()
            override fun isMainThread(): Boolean = true
        })
    }

    override fun afterEach(context: ExtensionContext?) {
        // Reset Coroutine Dispatcher.
        Dispatchers.resetMain()

        context?.root
            ?.getStore(TEST_COROUTINE_DISPATCHER_NAMESPACE)
            ?.get(TEST_COROUTINE_DISPATCHER_KEY, TestCoroutineDispatcher::class.java)!!
            .cleanupTestCoroutines()

        // Clear LiveData Executor
        ArchTaskExecutor.getInstance().setDelegate(null)
    }

    override fun supportsParameter(
        parameterContext: ParameterContext?,
        extensionContext: ExtensionContext?
    ) =
        parameterContext?.parameter?.type == TestCoroutineDispatcher::class.java ||
            parameterContext?.parameter?.type == ElectionDetailViewModel::class.java

    override fun resolveParameter(
        parameterContext: ParameterContext?,
        extensionContext: ExtensionContext?
    ): Any? =
        if (parameterContext?.parameter?.type == TestCoroutineDispatcher::class.java)
            getTestCoroutineDispatcher(extensionContext).let { dipatcher ->
                if (dipatcher == null) saveAndReturnTestCoroutineDispatcher(extensionContext)
                else dipatcher
            }
        else null

    private fun getTestCoroutineDispatcher(context: ExtensionContext?) = context?.root
        ?.getStore(TEST_COROUTINE_DISPATCHER_NAMESPACE)
        ?.get(TEST_COROUTINE_DISPATCHER_KEY, TestCoroutineDispatcher::class.java)

    private fun saveAndReturnTestCoroutineDispatcher(extensionContext: ExtensionContext?) =
        TestCoroutineDispatcher().apply {
            extensionContext?.root
                ?.getStore(TEST_COROUTINE_DISPATCHER_NAMESPACE)
                ?.put(TEST_COROUTINE_DISPATCHER_KEY, this)
        }
}
