package info.bitcoinunlimited.voter.election.detail.tests

import com.google.firebase.crashlytics.FirebaseCrashlytics
import info.bitcoinunlimited.voter.election.detail.ElectionDetailLoadElectionTest
import info.bitcoinunlimited.voter.election.detail.ElectionDetailTestExtensions
import info.bitcoinunlimited.voter.election.detail.testCases.loadElectionDetailTestCases
import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockkStatic
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
@ExtendWith(ElectionDetailTestExtensions::class)
class LoadElectionFinishedDetailTests(
    private val testDispatcher: TestCoroutineDispatcher
) {
    private fun electionLoad() = loadElectionDetailTestCases()

    @BeforeAll
    fun beforeAll() {
        mockkStatic(FirebaseCrashlytics::class)
    }

    @ParameterizedTest
    @MethodSource("electionLoad")
    fun `Election Detail`(test: ElectionDetailLoadElectionTest) {
        mockComponents(test)
    }

    private fun mockComponents(test: ElectionDetailLoadElectionTest) {
        val electionId = test.mockElection.id

        every { FirebaseCrashlytics.getInstance().log(any()) } just Runs
        every { FirebaseCrashlytics.getInstance().recordException(any()) } just Runs
    }
}
