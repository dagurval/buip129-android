package info.bitcoinunlimited.voter.election.master.tests

import app.coinverse.utils.getOrAwaitValue
import com.google.firebase.crashlytics.FirebaseCrashlytics
import info.bitcoinunlimited.voter.auth.AuthRepository
import info.bitcoinunlimited.voter.election.ElectionRepository
import info.bitcoinunlimited.voter.election.master.ElectionMasterLoadElectionsTest
import info.bitcoinunlimited.voter.election.master.ElectionMasterTestExtensions
import info.bitcoinunlimited.voter.election.master.testCases.loadElectionsIntoMasterTestCases
import info.bitcoinunlimited.voter.election.master.viewModel.ElectionMasterViewModel
import info.bitcoinunlimited.voter.election.master.viewModel.FeedViewEffectType
import info.bitcoinunlimited.voter.election.master.viewModel.FeedViewEventType
import info.bitcoinunlimited.voter.election.mockGetMainFeedList
import info.bitcoinunlimited.voter.election.mockQueryMainContentListLiveData
import info.bitcoinunlimited.voter.utils.FeedEventType
import info.bitcoinunlimited.voter.utils.Status
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockkClass
import io.mockk.mockkStatic
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.extension.ExtendWith

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
@ExtendWith(ElectionMasterTestExtensions::class)
class LoadElectionMasterTests(
    private val testDispatcher: TestCoroutineDispatcher
) {
    private fun electionFeedLoad() = loadElectionsIntoMasterTestCases()
    private val electionRepository = mockkClass(ElectionRepository::class)
    private val authRepository = mockkClass(AuthRepository::class)
    private lateinit var electionMasterViewModel: ElectionMasterViewModel

    @BeforeAll
    fun beforeAll() {
        // Android libraries
        mockkStatic(FirebaseCrashlytics::class)
    }

    // @ParameterizedTest
    // @MethodSource("electionFeedLoad")
    // fun `Election Selected`(test: ElectionMasterLoadElectionsTest) = testDispatcher.runBlockingTest {
    //     mockComponents(test)
    //     electionMasterViewModel = ElectionMasterViewModel(electionRepository, authRepository)
    //     FeedViewEventType.FeedLoad.also { event ->
    //         assertElectionList(test, FeedEventType.FEED_LOAD)
    //         verifyTests(test)
    //     }
    // }

    // TODO: FIXME https://gitlab.com/nerdekollektivet/buip129-android/-/issues/34
    // @ParameterizedTest
    // @MethodSource("electionFeedLoad")
    // fun `Feed Load`(test: ElectionMasterLoadElectionsTest) = testDispatcher.runBlockingTest {
    //     mockComponents(test)
    //     electionMasterViewModel = ElectionMasterViewModel(electionRepository, authRepository)
    //     electionMasterViewModel.getFeed(FeedViewEventType.FeedLoad)
    //     assertElectionList(test, FeedEventType.FEED_LOAD)
    //     verifyTests(test)
    // }

    // TODO: FIXME https://gitlab.com/nerdekollektivet/buip129-android/-/issues/34
    // @ParameterizedTest
    // @MethodSource("electionFeedLoad")
    // fun `Swipe to Refresh`(test: ElectionMasterLoadElectionsTest) = testDispatcher.runBlockingTest {
    //     mockComponents(test)
    //     electionMasterViewModel = ElectionMasterViewModel(electionRepository, authRepository)
    //     electionMasterViewModel.getFeed(FeedViewEventType.SwipeToRefresh)
    //     electionMasterViewModel.feedLoadComplete(FeedViewEventType.FeedLoadComplete(true))
    //     assertElectionList(test, FeedEventType.FEED_LOAD)
    // }

    private fun assertElectionList(
        test: ElectionMasterLoadElectionsTest,
        eventType: FeedEventType
    ) {

        // electionMasterViewModel.state.electionList.value = test.mockElectionList
        electionMasterViewModel.state.electionList.getOrAwaitValue().also { elections ->
            assertThat(elections).isEqualTo(test.mockElectionList)
            if (eventType == FeedEventType.SWIPE_TO_REFRESH) assertSwipeToRefresh(test)

            // ScreenEmptyEffect
            electionMasterViewModel.feedLoadComplete(FeedViewEventType.FeedLoadComplete(hasContent = elections.isNotEmpty()))
            electionMasterViewModel.effect.screenEmpty.getOrAwaitValue().also { effect ->
                assertThat(effect).isEqualTo(FeedViewEffectType.ScreenEmptyEffect(elections.isEmpty()))
            }
        }
    }

    private fun assertSwipeToRefresh(test: ElectionMasterLoadElectionsTest) {
        when (test.status) {
            Status.LOADING -> electionMasterViewModel.effect.swipeToRefresh.getOrAwaitValue().also { effect ->
                assertThat(effect).isEqualTo(FeedViewEffectType.SwipeToRefreshEffect(true))
            }
            Status.SUCCESS -> electionMasterViewModel.effect.swipeToRefresh.getOrAwaitValue().also { effect ->
                assertThat(effect).isEqualTo(FeedViewEffectType.SwipeToRefreshEffect(false))
            }
            Status.ERROR -> electionMasterViewModel.effect.swipeToRefresh.getOrAwaitValue().also { effect ->
                assertThat(effect).isEqualTo(FeedViewEffectType.SwipeToRefreshEffect(false))
            }
        }
    }

    private fun mockComponents(test: ElectionMasterLoadElectionsTest) {

        // Android libraries
        every { FirebaseCrashlytics.getInstance().log(any()) } returns Unit

        // ElectionRepository
        coEvery {
            electionRepository.getElectionsNetwork(any())
        } returns mockGetMainFeedList(test.mockElectionList, test.status)
        every {
            electionRepository.getElectionsRoom()
        } returns mockQueryMainContentListLiveData(test.mockElectionList)

        // AuthRepository
        every {
            authRepository.getCurrentUserId()
        } returns "mockUserid"
    }

    private fun verifyTests(test: ElectionMasterLoadElectionsTest) {
        coVerify {
            electionRepository.getElectionsNetwork(any())
            if (test.status == Status.LOADING || test.status == Status.ERROR)
                electionRepository.getElectionsRoom()
        }

        confirmVerified(electionRepository)
    }
}
