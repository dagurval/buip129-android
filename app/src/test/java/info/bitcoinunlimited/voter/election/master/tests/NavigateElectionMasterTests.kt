package info.bitcoinunlimited.voter.election.master.tests

import app.coinverse.utils.getOrAwaitValue
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.crashlytics.FirebaseCrashlytics
import info.bitcoinunlimited.voter.auth.AuthRepository
import info.bitcoinunlimited.voter.election.ElectionRepository
import info.bitcoinunlimited.voter.election.master.ElectionMasterTestExtensions
import info.bitcoinunlimited.voter.election.master.NavigateElectionMasterTest
import info.bitcoinunlimited.voter.election.master.testCases.navigateElectionListTestCases
import info.bitcoinunlimited.voter.election.master.viewModel.ElectionMasterViewModel
import info.bitcoinunlimited.voter.election.master.viewModel.FeedViewEventType
import info.bitcoinunlimited.voter.election.mockGetMainFeedList
import info.bitcoinunlimited.voter.election.mockQueryMainContentListLiveData
import info.bitcoinunlimited.voter.utils.Status
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.just
import io.mockk.mockkClass
import io.mockk.mockkStatic
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
@ExtendWith(ElectionMasterTestExtensions::class)
class NavigateElectionMasterTests(
    private val testDispatcher: TestCoroutineDispatcher
) {

    private fun navigateElectionMasterDetail() = navigateElectionListTestCases()
    private lateinit var electionMasterViewModel: ElectionMasterViewModel
    private val electionRepository = mockkClass(ElectionRepository::class)
    private val authRepository = mockkClass(AuthRepository::class)

    @BeforeAll
    fun beforeAll() {
        mockkStatic(FirebaseCrashlytics::class)
        mockkStatic(FirebaseAuth::class)
    }

    @ParameterizedTest
    @MethodSource("navigateElectionMasterDetail")
    fun `Navigate to Election`(test: NavigateElectionMasterTest) = testDispatcher.runBlockingTest {
        mockComponents(test)
        electionMasterViewModel = ElectionMasterViewModel(electionRepository, authRepository)

        FeedViewEventType.FeedLoad.also { event ->
            electionMasterViewModel.getFeed(event)
            assertElectionList(test)
        }

        verifyTests(test)
    }

    private fun mockComponents(test: NavigateElectionMasterTest) {
        val electionId = test.mockElection.id

        // Android libraries
        every { FirebaseCrashlytics.getInstance().log(any()) } just Runs
        every { FirebaseCrashlytics.getInstance().recordException(any()) } just Runs

        // ElectionRepository
        coEvery {
            electionRepository.getElectionsNetwork(any())
        } returns mockGetMainFeedList(test.mockElectionList, test.status)
        every {
            electionRepository.getElectionsRoom()
        } returns mockQueryMainContentListLiveData(test.mockElectionList)

        // AuthRepository
        every {
            authRepository.getCurrentUserId()
        } returns "mockUserid"
    }

    private fun assertElectionList(test: NavigateElectionMasterTest) {
        electionMasterViewModel.state.electionList.getOrAwaitValue().also { electionList ->
            assertThat(electionList).isEqualTo(test.mockElectionList)
        }
    }

    private fun verifyTests(test: NavigateElectionMasterTest) {
        coVerify {
            electionRepository.getElectionsNetwork(any())
            if (test.status == Status.LOADING || test.status == Status.ERROR)
                electionRepository.getElectionsRoom()
        }
        confirmVerified(electionRepository)
    }
}
