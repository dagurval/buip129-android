package info.bitcoinunlimited.voter.utils

enum class Status { LOADING, SUCCESS, ERROR }

enum class FeedEventType { FEED_LOAD, SWIPE_TO_REFRESH }
