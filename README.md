# Voter.cash android voting

## Contributing

### Dependencies

You need `libbitcoincashkotlin` from https://gitlab.com/nerdekollektivet/

Clone the in the projects root directory, or glone it somewhere else and symlink
it.

### Android Studio

To make Intellij IDEA's built-in formatter produce 100% ktlint-compatible code, run once

`./gradlew ktlintApplyToIDEAProject`

## Architecure
Reference: https://proandroiddev.com/android-unidirectional-data-flow-local-unit-testing-487a6e6f5c9
![Image description](https://miro.medium.com/max/960/1*Rk7winDtAjRkYw8uKkeWhA.png)

## Generate docs:
./gradlew dokka

## Test with coverage report:
./gradlew unitTestAndCreateDebugCoverageReport
./gradlew jacocoTestReport
